/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Allergy;
import entity.Desease;
import entity.Deseasestatus;
import entity.Faculty;
import entity.Injury;
import entity.Level;
import entity.Swimmer;
import entity.SwimmerHasAllergy;
import entity.SwimmerHasDesease;
import entity.SwimmerHasInjury;
import entity.SwimmerHasLevel;
import entity.SwimmerHasSwimmingclub;
import entity.Swimmingclub;
import entity.User;
import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import session.AllergyFacade;
import session.DeseaseFacade;
import session.DeseasestatusFacade;
import session.InjuryFacade;
import session.LevelFacade;
import session.SwimmerFacade;
import session.SwimmerHasAllergyFacade;
import session.SwimmerHasDeseaseFacade;
import session.SwimmerHasInjuryFacade;
import session.SwimmerHasLevelFacade;
import session.SwimmerHasSwimmingclubFacade;
import session.SwimmingclubFacade;
import session.UserFacade;

/**
 *
 * @author enriq
 */
@Named(value = "swimmersController")
@Dependent
public class SwimmerController {

    //VARIABLES
    //Selected swimmer variables
    private Faculty selectedFaculty = new Faculty();
    private User user = new User();
    private Swimmer swimmer = new Swimmer();
    
    //Selected swimmer characteristics
    private entity.Level selectedLevel = new entity.Level();
    private Swimmingclub selectedSwimmingClub = new Swimmingclub();
    private Injury selectedInjury = new Injury();
    private Allergy selectedAllergy = new Allergy();
    private Desease selectedDesease = new Desease();
    private Deseasestatus selectedDeseaseStatus = new Deseasestatus();
    
    //Dates values
  
    private Date allergyDetectionDate;
    
    //Many to many objects type Has ofor swimmer
    private SwimmerHasAllergy swimmerHasAllergy = new SwimmerHasAllergy();
    private SwimmerHasDesease swimmerHasDesease = new SwimmerHasDesease();
    private SwimmerHasLevel swimmerHasLevel = new SwimmerHasLevel();
    private SwimmerHasInjury swimmerHasInjury = new SwimmerHasInjury();
    private SwimmerHasSwimmingclub swimmerHasSwimmingclub = new SwimmerHasSwimmingclub();


    //INJECTIONS
    @Inject
    private UserFacade uf;
    @Inject
    private SwimmerFacade sf;

    //Catalogue Injections
    @Inject
    private LevelFacade lf;
    @Inject
    private SwimmingclubFacade scf;
    @Inject
    private InjuryFacade inf;
    @Inject
    private AllergyFacade af;
    @Inject
    private DeseaseFacade df;
    @Inject
    private DeseasestatusFacade def;

    //Properties of swimmer tables Injection
    @Inject
    private SwimmerHasLevelFacade swimmerHasLevelFacade;
    @Inject
    private SwimmerHasSwimmingclubFacade swimmerHasSwimmingclubFacade;
    @Inject
    private SwimmerHasInjuryFacade swimmerHasInjuryFacade;
    @Inject
    private SwimmerHasAllergyFacade swimmerHasAllergyFacade;
    @Inject
    private SwimmerHasDeseaseFacade swimmerHasDeseaseFacade;
    
    
 
    

    /**
     * Creates a new instance of SwimmerController
     */
    public SwimmerController() {
        
    }

    //CRUD METHODS  
    public List<Swimmer> findAllSwimmers() {
        return this.sf.findAll();
    }
    
    public void CreateSwimmer(){
        //PERSONAL INFORMATION
        uf.create(this.user);                                                                                                           //Persisting user data
         
        this.swimmer.setUserid(this.user);
        this.swimmer.setFacultyid(selectedFaculty);
        sf.create(this.swimmer);                                                                                                    //Persisting swimmer data
        
         //SWIMMER CHARACTERISTICS
            //Level
            this.swimmerHasLevel.setSwimmerid(this.swimmer);
            this.swimmerHasLevel.setLevelid(this.selectedLevel);
            swimmerHasLevelFacade.create(this.swimmerHasLevel);                                               //Persisting swimmer Level
            
            //Swimming Club
            this.swimmerHasSwimmingclub.setSwimmerid(swimmer);
            this.swimmerHasSwimmingclub.setSwimmingClubid(this.selectedSwimmingClub);
            this.swimmerHasSwimmingclubFacade.create(this.swimmerHasSwimmingclub);
            
            //Injury
            this.swimmerHasInjury.setSwimmerId(swimmer);
            this.swimmerHasInjury.setInjuryId(selectedInjury);
            this.swimmerHasInjuryFacade.create(swimmerHasInjury);                                             //Persisting Injury
            
            //Allergy
            this.swimmerHasAllergy.setSwimmerId(swimmer);
            this.swimmerHasAllergy.setAllergyId(this.selectedAllergy);
            this.swimmerHasAllergyFacade.create(this.swimmerHasAllergy);                                    //Persisting allergy
            
            //Desease
            this.swimmerHasDesease.setSwimmerId(swimmer);
            this.swimmerHasDesease.setDeseaseId(selectedDesease);
            this.swimmerHasDesease.setDeseaseStatusid(selectedDeseaseStatus);
            this.swimmerHasDeseaseFacade.create(swimmerHasDesease);                                 //Persisting Desease
            
    }

    //ASSISTANT METHODS
    
    //Catalogue Methods
    public List<entity.Level> findLevels(){
    return lf.findAll();
    }
    
    public List<Swimmingclub> findSwimmingClubs(){
    return this.scf.findAll();
    }
    
    public List<Injury> findInjuries(){
    return this.inf.findAll();
    }
    
    public List<Allergy> findAllergies(){
    return this.af.findAll();
    }
    
    public List<Desease> findDeseases(){
    return this.df.findAll();
    }
    
    public List<Deseasestatus> findDeseaseStatuses(){
    return this.def.findAll();
    }
    
    
    //AUXILARY  METHODS
    //GETTERS AND SETTERS
    public Swimmer getSwimmer() {
        return swimmer;
    }

    public void setSwimmer(Swimmer swimmer) {
        this.swimmer = swimmer;
    }

    public Level getSelectedLevel() {
        return selectedLevel;
    }

    public void setSelectedLevel(Level selectedLevel) {
        this.selectedLevel = selectedLevel;
    }

    public Swimmingclub getSelectedSwimmingClub() {
        return selectedSwimmingClub;
    }

    public void setSelectedSwimmingClub(Swimmingclub selectedSwimmingClub) {
        this.selectedSwimmingClub = selectedSwimmingClub;
    }

    public Injury getSelectedInjury() {
        return selectedInjury;
    }

    public void setSelectedInjury(Injury selectedInjury) {
        this.selectedInjury = selectedInjury;
    }

    public Allergy getSelectedAllergy() {
        return selectedAllergy;
    }

    public void setSelectedAllergy(Allergy selectedAllergy) {
        this.selectedAllergy = selectedAllergy;
    }

    public Desease getSelectedDesease() {
        return selectedDesease;
    }

    public void setSelectedDesease(Desease selectedDesease) {
        this.selectedDesease = selectedDesease;
    }

    public Deseasestatus getSelectedDeseaseStatus() {
        return selectedDeseaseStatus;
    }

    public void setSelectedDeseaseStatus(Deseasestatus selectedDeseaseStatus) {
        this.selectedDeseaseStatus = selectedDeseaseStatus;
    }
    
    

}
