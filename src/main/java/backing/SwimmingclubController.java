package backing;

import backing.util.MobilePageController;
import entity.Swimmingclub;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "swimmingclubController")
@ViewScoped
public class SwimmingclubController extends AbstractController<Swimmingclub> {

    @Inject
    private MobilePageController mobilePageController;

    public SwimmingclubController() {
        // Inform the Abstract parent controller of the concrete Swimmingclub Entity
        super(Swimmingclub.class);
    }

    /**
     * Sets the "items" attribute with a collection of SwimmerHasSwimmingclub
     * entities that are retrieved from Swimmingclub?cap_first and returns the
     * navigation outcome.
     *
     * @return navigation outcome for SwimmerHasSwimmingclub page
     */
    public String navigateSwimmerHasSwimmingclubList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SwimmerHasSwimmingclub_items", this.getSelected().getSwimmerHasSwimmingclubList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmerHasSwimmingclub/index";
    }

}
