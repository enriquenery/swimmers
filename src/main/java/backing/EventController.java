package backing;

import backing.util.MobilePageController;
import entity.Event;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "eventController")
@ViewScoped
public class EventController extends AbstractController<Event> {

    @Inject
    private ChampionshipController championshipidController;
    @Inject
    private RaceController raceidController;
    @Inject
    private MobilePageController mobilePageController;

    public EventController() {
        // Inform the Abstract parent controller of the concrete Event Entity
        super(Event.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        championshipidController.setSelected(null);
        raceidController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Hit entities that are
     * retrieved from Event?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Hit page
     */
    public String navigateHitList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Hit_items", this.getSelected().getHitList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/hit/index";
    }

    /**
     * Sets the "selected" attribute of the Championship controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareChampionshipid(ActionEvent event) {
        if (this.getSelected() != null && championshipidController.getSelected() == null) {
            championshipidController.setSelected(this.getSelected().getChampionshipid());
        }
    }

    /**
     * Sets the "selected" attribute of the Race controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareRaceid(ActionEvent event) {
        if (this.getSelected() != null && raceidController.getSelected() == null) {
            raceidController.setSelected(this.getSelected().getRaceid());
        }
    }
}
