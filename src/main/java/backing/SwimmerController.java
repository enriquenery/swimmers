package backing;

import backing.util.MobilePageController;
import entity.Swimmer;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "swimmerController")
@ViewScoped
public class SwimmerController extends AbstractController<Swimmer> {

    @Inject
    private FacultyController facultyidController;
    @Inject
    private UserController useridController;
    @Inject
    private MobilePageController mobilePageController;

    public SwimmerController() {
        // Inform the Abstract parent controller of the concrete Swimmer Entity
        super(Swimmer.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        facultyidController.setSelected(null);
        useridController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of SwimmerHasDesease
     * entities that are retrieved from Swimmer?cap_first and returns the
     * navigation outcome.
     *
     * @return navigation outcome for SwimmerHasDesease page
     */
    public String navigateSwimmerHasDeseaseList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SwimmerHasDesease_items", this.getSelected().getSwimmerHasDeseaseList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmerHasDesease/index";
    }

    /**
     * Sets the "items" attribute with a collection of Hit entities that are
     * retrieved from Swimmer?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Hit page
     */
    public String navigateHitList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Hit_items", this.getSelected().getHitList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/hit/index";
    }

    /**
     * Sets the "items" attribute with a collection of SwimmerHasSwimmingclub
     * entities that are retrieved from Swimmer?cap_first and returns the
     * navigation outcome.
     *
     * @return navigation outcome for SwimmerHasSwimmingclub page
     */
    public String navigateSwimmerHasSwimmingclubList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SwimmerHasSwimmingclub_items", this.getSelected().getSwimmerHasSwimmingclubList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmerHasSwimmingclub/index";
    }

    /**
     * Sets the "items" attribute with a collection of SwimmerHasAllergy
     * entities that are retrieved from Swimmer?cap_first and returns the
     * navigation outcome.
     *
     * @return navigation outcome for SwimmerHasAllergy page
     */
    public String navigateSwimmerHasAllergyList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SwimmerHasAllergy_items", this.getSelected().getSwimmerHasAllergyList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmerHasAllergy/index";
    }

    /**
     * Sets the "items" attribute with a collection of SwimmerHasInjury entities
     * that are retrieved from Swimmer?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for SwimmerHasInjury page
     */
    public String navigateSwimmerHasInjuryList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SwimmerHasInjury_items", this.getSelected().getSwimmerHasInjuryList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmerHasInjury/index";
    }

    /**
     * Sets the "selected" attribute of the Faculty controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareFacultyid(ActionEvent event) {
        if (this.getSelected() != null && facultyidController.getSelected() == null) {
            facultyidController.setSelected(this.getSelected().getFacultyid());
        }
    }

    /**
     * Sets the "selected" attribute of the User controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareUserid(ActionEvent event) {
        if (this.getSelected() != null && useridController.getSelected() == null) {
            useridController.setSelected(this.getSelected().getUserid());
        }
    }

    /**
     * Sets the "items" attribute with a collection of Imc entities that are
     * retrieved from Swimmer?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Imc page
     */
    public String navigateImcList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Imc_items", this.getSelected().getImcList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/imc/index";
    }

    /**
     * Sets the "items" attribute with a collection of SwimmerHasLevel entities
     * that are retrieved from Swimmer?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for SwimmerHasLevel page
     */
    public String navigateSwimmerHasLevelList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SwimmerHasLevel_items", this.getSelected().getSwimmerHasLevelList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmerHasLevel/index";
    }

}
