package backing;

import backing.util.MobilePageController;
import entity.Hitstatus;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "hitstatusController")
@ViewScoped
public class HitstatusController extends AbstractController<Hitstatus> {

    @Inject
    private MobilePageController mobilePageController;

    public HitstatusController() {
        // Inform the Abstract parent controller of the concrete Hitstatus Entity
        super(Hitstatus.class);
    }

    /**
     * Sets the "items" attribute with a collection of Hit entities that are
     * retrieved from Hitstatus?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Hit page
     */
    public String navigateHitList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Hit_items", this.getSelected().getHitList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/hit/index";
    }

}
