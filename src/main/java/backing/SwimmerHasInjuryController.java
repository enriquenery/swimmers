package backing;

import backing.util.MobilePageController;
import entity.SwimmerHasInjury;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "swimmerHasInjuryController")
@ViewScoped
public class SwimmerHasInjuryController extends AbstractController<SwimmerHasInjury> {

    @Inject
    private InjuryController injuryIdController;
    @Inject
    private InjurystatusController injuryStatusidController;
    @Inject
    private SwimmerController swimmerIdController;
    @Inject
    private MobilePageController mobilePageController;

    public SwimmerHasInjuryController() {
        // Inform the Abstract parent controller of the concrete SwimmerHasInjury Entity
        super(SwimmerHasInjury.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        injuryIdController.setSelected(null);
        injuryStatusidController.setSelected(null);
        swimmerIdController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Injury controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareInjuryId(ActionEvent event) {
        if (this.getSelected() != null && injuryIdController.getSelected() == null) {
            injuryIdController.setSelected(this.getSelected().getInjuryId());
        }
    }

    /**
     * Sets the "selected" attribute of the Injurystatus controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareInjuryStatusid(ActionEvent event) {
        if (this.getSelected() != null && injuryStatusidController.getSelected() == null) {
            injuryStatusidController.setSelected(this.getSelected().getInjuryStatusid());
        }
    }

    /**
     * Sets the "selected" attribute of the Swimmer controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareSwimmerId(ActionEvent event) {
        if (this.getSelected() != null && swimmerIdController.getSelected() == null) {
            swimmerIdController.setSelected(this.getSelected().getSwimmerId());
        }
    }
}
