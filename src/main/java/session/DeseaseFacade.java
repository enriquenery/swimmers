/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Desease;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author enriq
 */
@Stateless
public class DeseaseFacade extends AbstractFacade<Desease> {

    @PersistenceContext(unitName = "sv.edu.ues.natacion_swimmers_war_0.1PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DeseaseFacade() {
        super(Desease.class);
    }
    
}
