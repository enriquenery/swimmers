package backing;

import backing.util.MobilePageController;
import entity.Allergy;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "allergyController")
@ViewScoped
public class AllergyController extends AbstractController<Allergy> {

    @Inject
    private MobilePageController mobilePageController;

    public AllergyController() {
        // Inform the Abstract parent controller of the concrete Allergy Entity
        super(Allergy.class);
    }

    /**
     * Sets the "items" attribute with a collection of SwimmerHasAllergy
     * entities that are retrieved from Allergy?cap_first and returns the
     * navigation outcome.
     *
     * @return navigation outcome for SwimmerHasAllergy page
     */
    public String navigateSwimmerHasAllergyList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SwimmerHasAllergy_items", this.getSelected().getSwimmerHasAllergyList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmerHasAllergy/index";
    }

}
