/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "imc")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Imc.findAll", query = "SELECT i FROM Imc i")
    , @NamedQuery(name = "Imc.findById", query = "SELECT i FROM Imc i WHERE i.id = :id")
    , @NamedQuery(name = "Imc.findByDate", query = "SELECT i FROM Imc i WHERE i.date = :date")
    , @NamedQuery(name = "Imc.findByWeight", query = "SELECT i FROM Imc i WHERE i.weight = :weight")
    , @NamedQuery(name = "Imc.findByHeight", query = "SELECT i FROM Imc i WHERE i.height = :height")
    , @NamedQuery(name = "Imc.findByImc", query = "SELECT i FROM Imc i WHERE i.imc = :imc")})
public class Imc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "weight")
    private BigDecimal weight;
    @Column(name = "height")
    private BigDecimal height;
    @Column(name = "imc")
    private BigDecimal imc;
    @JoinColumn(name = "Swimmer_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Swimmer swimmerid;

    public Imc() {
    }

    public Imc(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public BigDecimal getImc() {
        return imc;
    }

    public void setImc(BigDecimal imc) {
        this.imc = imc;
    }

    public Swimmer getSwimmerid() {
        return swimmerid;
    }

    public void setSwimmerid(Swimmer swimmerid) {
        this.swimmerid = swimmerid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Imc)) {
            return false;
        }
        Imc other = (Imc) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Imc[ id=" + id + " ]";
    }
    
}
