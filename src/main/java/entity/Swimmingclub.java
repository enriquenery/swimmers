/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "swimmingclub")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Swimmingclub.findAll", query = "SELECT s FROM Swimmingclub s")
    , @NamedQuery(name = "Swimmingclub.findById", query = "SELECT s FROM Swimmingclub s WHERE s.id = :id")
    , @NamedQuery(name = "Swimmingclub.findByClubName", query = "SELECT s FROM Swimmingclub s WHERE s.clubName = :clubName")
    , @NamedQuery(name = "Swimmingclub.findByClubDescription", query = "SELECT s FROM Swimmingclub s WHERE s.clubDescription = :clubDescription")})
public class Swimmingclub implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "clubName")
    private String clubName;
    @Size(max = 200)
    @Column(name = "clubDescription")
    private String clubDescription;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "swimmingClubid")
    private List<SwimmerHasSwimmingclub> swimmerHasSwimmingclubList;

    public Swimmingclub() {
    }

    public Swimmingclub(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getClubDescription() {
        return clubDescription;
    }

    public void setClubDescription(String clubDescription) {
        this.clubDescription = clubDescription;
    }

    @XmlTransient
    public List<SwimmerHasSwimmingclub> getSwimmerHasSwimmingclubList() {
        return swimmerHasSwimmingclubList;
    }

    public void setSwimmerHasSwimmingclubList(List<SwimmerHasSwimmingclub> swimmerHasSwimmingclubList) {
        this.swimmerHasSwimmingclubList = swimmerHasSwimmingclubList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Swimmingclub)) {
            return false;
        }
        Swimmingclub other = (Swimmingclub) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Swimmingclub[ id=" + id + " ]";
    }
    
}
