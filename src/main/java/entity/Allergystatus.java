/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "allergystatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Allergystatus.findAll", query = "SELECT a FROM Allergystatus a")
    , @NamedQuery(name = "Allergystatus.findById", query = "SELECT a FROM Allergystatus a WHERE a.id = :id")
    , @NamedQuery(name = "Allergystatus.findByName", query = "SELECT a FROM Allergystatus a WHERE a.name = :name")
    , @NamedQuery(name = "Allergystatus.findByDescription", query = "SELECT a FROM Allergystatus a WHERE a.description = :description")})
public class Allergystatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @Size(max = 200)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "allergyStatusid")
    private List<SwimmerHasAllergy> swimmerHasAllergyList;

    public Allergystatus() {
    }

    public Allergystatus(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public List<SwimmerHasAllergy> getSwimmerHasAllergyList() {
        return swimmerHasAllergyList;
    }

    public void setSwimmerHasAllergyList(List<SwimmerHasAllergy> swimmerHasAllergyList) {
        this.swimmerHasAllergyList = swimmerHasAllergyList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Allergystatus)) {
            return false;
        }
        Allergystatus other = (Allergystatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Allergystatus[ id=" + id + " ]";
    }
    
}
