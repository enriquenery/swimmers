/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "swimmer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Swimmer.findAll", query = "SELECT s FROM Swimmer s")
    , @NamedQuery(name = "Swimmer.findById", query = "SELECT s FROM Swimmer s WHERE s.id = :id")})
public class Swimmer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "swimmerId")
    private List<SwimmerHasDesease> swimmerHasDeseaseList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "swimmerid")
    private List<Hit> hitList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "swimmerid")
    private List<SwimmerHasSwimmingclub> swimmerHasSwimmingclubList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "swimmerId")
    private List<SwimmerHasAllergy> swimmerHasAllergyList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "swimmerId")
    private List<SwimmerHasInjury> swimmerHasInjuryList;
    @JoinColumn(name = "Faculty_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Faculty facultyid;
    @JoinColumn(name = "User_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User userid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "swimmerid")
    private List<Imc> imcList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "swimmerid")
    private List<SwimmerHasLevel> swimmerHasLevelList;

    public Swimmer() {
    }

    public Swimmer(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public List<SwimmerHasDesease> getSwimmerHasDeseaseList() {
        return swimmerHasDeseaseList;
    }

    public void setSwimmerHasDeseaseList(List<SwimmerHasDesease> swimmerHasDeseaseList) {
        this.swimmerHasDeseaseList = swimmerHasDeseaseList;
    }

    @XmlTransient
    public List<Hit> getHitList() {
        return hitList;
    }

    public void setHitList(List<Hit> hitList) {
        this.hitList = hitList;
    }

    @XmlTransient
    public List<SwimmerHasSwimmingclub> getSwimmerHasSwimmingclubList() {
        return swimmerHasSwimmingclubList;
    }

    public void setSwimmerHasSwimmingclubList(List<SwimmerHasSwimmingclub> swimmerHasSwimmingclubList) {
        this.swimmerHasSwimmingclubList = swimmerHasSwimmingclubList;
    }

    @XmlTransient
    public List<SwimmerHasAllergy> getSwimmerHasAllergyList() {
        return swimmerHasAllergyList;
    }

    public void setSwimmerHasAllergyList(List<SwimmerHasAllergy> swimmerHasAllergyList) {
        this.swimmerHasAllergyList = swimmerHasAllergyList;
    }

    @XmlTransient
    public List<SwimmerHasInjury> getSwimmerHasInjuryList() {
        return swimmerHasInjuryList;
    }

    public void setSwimmerHasInjuryList(List<SwimmerHasInjury> swimmerHasInjuryList) {
        this.swimmerHasInjuryList = swimmerHasInjuryList;
    }

    public Faculty getFacultyid() {
        return facultyid;
    }

    public void setFacultyid(Faculty facultyid) {
        this.facultyid = facultyid;
    }

    public User getUserid() {
        return userid;
    }

    public void setUserid(User userid) {
        this.userid = userid;
    }

    @XmlTransient
    public List<Imc> getImcList() {
        return imcList;
    }

    public void setImcList(List<Imc> imcList) {
        this.imcList = imcList;
    }

    @XmlTransient
    public List<SwimmerHasLevel> getSwimmerHasLevelList() {
        return swimmerHasLevelList;
    }

    public void setSwimmerHasLevelList(List<SwimmerHasLevel> swimmerHasLevelList) {
        this.swimmerHasLevelList = swimmerHasLevelList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Swimmer)) {
            return false;
        }
        Swimmer other = (Swimmer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Swimmer[ id=" + id + " ]";
    }
    
}
