package backing;

import backing.util.MobilePageController;
import entity.Level;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "levelController")
@ViewScoped
public class LevelController extends AbstractController<Level> {

    @Inject
    private MobilePageController mobilePageController;

    public LevelController() {
        // Inform the Abstract parent controller of the concrete Level Entity
        super(Level.class);
    }

    /**
     * Sets the "items" attribute with a collection of SwimmerHasLevel entities
     * that are retrieved from Level?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for SwimmerHasLevel page
     */
    public String navigateSwimmerHasLevelList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SwimmerHasLevel_items", this.getSelected().getSwimmerHasLevelList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmerHasLevel/index";
    }

}
