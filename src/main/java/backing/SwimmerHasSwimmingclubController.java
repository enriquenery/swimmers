package backing;

import backing.util.MobilePageController;
import entity.SwimmerHasSwimmingclub;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "swimmerHasSwimmingclubController")
@ViewScoped
public class SwimmerHasSwimmingclubController extends AbstractController<SwimmerHasSwimmingclub> {

    @Inject
    private SwimmerController swimmeridController;
    @Inject
    private SwimmingclubController swimmingClubidController;
    @Inject
    private MobilePageController mobilePageController;

    public SwimmerHasSwimmingclubController() {
        // Inform the Abstract parent controller of the concrete SwimmerHasSwimmingclub Entity
        super(SwimmerHasSwimmingclub.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        swimmeridController.setSelected(null);
        swimmingClubidController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Swimmer controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareSwimmerid(ActionEvent event) {
        if (this.getSelected() != null && swimmeridController.getSelected() == null) {
            swimmeridController.setSelected(this.getSelected().getSwimmerid());
        }
    }

    /**
     * Sets the "selected" attribute of the Swimmingclub controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareSwimmingClubid(ActionEvent event) {
        if (this.getSelected() != null && swimmingClubidController.getSelected() == null) {
            swimmingClubidController.setSelected(this.getSelected().getSwimmingClubid());
        }
    }
}
