/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "swimmer_has_swimmingclub")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SwimmerHasSwimmingclub.findAll", query = "SELECT s FROM SwimmerHasSwimmingclub s")
    , @NamedQuery(name = "SwimmerHasSwimmingclub.findById", query = "SELECT s FROM SwimmerHasSwimmingclub s WHERE s.id = :id")
    , @NamedQuery(name = "SwimmerHasSwimmingclub.findByDateStart", query = "SELECT s FROM SwimmerHasSwimmingclub s WHERE s.dateStart = :dateStart")
    , @NamedQuery(name = "SwimmerHasSwimmingclub.findByDateEnd", query = "SELECT s FROM SwimmerHasSwimmingclub s WHERE s.dateEnd = :dateEnd")})
public class SwimmerHasSwimmingclub implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "dateStart")
    @Temporal(TemporalType.DATE)
    private Date dateStart;
    @Column(name = "dateEnd")
    @Temporal(TemporalType.DATE)
    private Date dateEnd;
    @JoinColumn(name = "Swimmer_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Swimmer swimmerid;
    @JoinColumn(name = "swimmingClub_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Swimmingclub swimmingClubid;

    public SwimmerHasSwimmingclub() {
    }

    public SwimmerHasSwimmingclub(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Swimmer getSwimmerid() {
        return swimmerid;
    }

    public void setSwimmerid(Swimmer swimmerid) {
        this.swimmerid = swimmerid;
    }

    public Swimmingclub getSwimmingClubid() {
        return swimmingClubid;
    }

    public void setSwimmingClubid(Swimmingclub swimmingClubid) {
        this.swimmingClubid = swimmingClubid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SwimmerHasSwimmingclub)) {
            return false;
        }
        SwimmerHasSwimmingclub other = (SwimmerHasSwimmingclub) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.SwimmerHasSwimmingclub[ id=" + id + " ]";
    }
    
}
