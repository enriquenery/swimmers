package backing;

import backing.util.MobilePageController;
import entity.SwimmerHasAllergy;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "swimmerHasAllergyController")
@ViewScoped
public class SwimmerHasAllergyController extends AbstractController<SwimmerHasAllergy> {

    @Inject
    private AllergyController allergyIdController;
    @Inject
    private AllergystatusController allergyStatusidController;
    @Inject
    private SwimmerController swimmerIdController;
    @Inject
    private MobilePageController mobilePageController;

    public SwimmerHasAllergyController() {
        // Inform the Abstract parent controller of the concrete SwimmerHasAllergy Entity
        super(SwimmerHasAllergy.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        allergyIdController.setSelected(null);
        allergyStatusidController.setSelected(null);
        swimmerIdController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Allergy controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareAllergyId(ActionEvent event) {
        if (this.getSelected() != null && allergyIdController.getSelected() == null) {
            allergyIdController.setSelected(this.getSelected().getAllergyId());
        }
    }

    /**
     * Sets the "selected" attribute of the Allergystatus controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareAllergyStatusid(ActionEvent event) {
        if (this.getSelected() != null && allergyStatusidController.getSelected() == null) {
            allergyStatusidController.setSelected(this.getSelected().getAllergyStatusid());
        }
    }

    /**
     * Sets the "selected" attribute of the Swimmer controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareSwimmerId(ActionEvent event) {
        if (this.getSelected() != null && swimmerIdController.getSelected() == null) {
            swimmerIdController.setSelected(this.getSelected().getSwimmerId());
        }
    }
}
