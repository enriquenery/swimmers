package backing;

import backing.util.MobilePageController;
import entity.Faculty;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "facultyController")
@ViewScoped
public class FacultyController extends AbstractController<Faculty> {

    @Inject
    private MobilePageController mobilePageController;

    public FacultyController() {
        // Inform the Abstract parent controller of the concrete Faculty Entity
        super(Faculty.class);
    }

    /**
     * Sets the "items" attribute with a collection of Swimmer entities that are
     * retrieved from Faculty?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Swimmer page
     */
    public String navigateSwimmerList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Swimmer_items", this.getSelected().getSwimmerList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmer/index";
    }

}
