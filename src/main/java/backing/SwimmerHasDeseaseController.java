package backing;

import backing.util.MobilePageController;
import entity.SwimmerHasDesease;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "swimmerHasDeseaseController")
@ViewScoped
public class SwimmerHasDeseaseController extends AbstractController<SwimmerHasDesease> {

    @Inject
    private DeseaseController deseaseIdController;
    @Inject
    private DeseasestatusController deseaseStatusidController;
    @Inject
    private SwimmerController swimmerIdController;
    @Inject
    private MobilePageController mobilePageController;

    public SwimmerHasDeseaseController() {
        // Inform the Abstract parent controller of the concrete SwimmerHasDesease Entity
        super(SwimmerHasDesease.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        deseaseIdController.setSelected(null);
        deseaseStatusidController.setSelected(null);
        swimmerIdController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Desease controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareDeseaseId(ActionEvent event) {
        if (this.getSelected() != null && deseaseIdController.getSelected() == null) {
            deseaseIdController.setSelected(this.getSelected().getDeseaseId());
        }
    }

    /**
     * Sets the "selected" attribute of the Deseasestatus controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareDeseaseStatusid(ActionEvent event) {
        if (this.getSelected() != null && deseaseStatusidController.getSelected() == null) {
            deseaseStatusidController.setSelected(this.getSelected().getDeseaseStatusid());
        }
    }

    /**
     * Sets the "selected" attribute of the Swimmer controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareSwimmerId(ActionEvent event) {
        if (this.getSelected() != null && swimmerIdController.getSelected() == null) {
            swimmerIdController.setSelected(this.getSelected().getSwimmerId());
        }
    }
}
