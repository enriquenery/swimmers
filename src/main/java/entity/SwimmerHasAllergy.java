/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "swimmer_has_allergy")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SwimmerHasAllergy.findAll", query = "SELECT s FROM SwimmerHasAllergy s")
    , @NamedQuery(name = "SwimmerHasAllergy.findById", query = "SELECT s FROM SwimmerHasAllergy s WHERE s.id = :id")
    , @NamedQuery(name = "SwimmerHasAllergy.findByAllergyDetectionDate", query = "SELECT s FROM SwimmerHasAllergy s WHERE s.allergyDetectionDate = :allergyDetectionDate")
    , @NamedQuery(name = "SwimmerHasAllergy.findByObservations", query = "SELECT s FROM SwimmerHasAllergy s WHERE s.observations = :observations")})
public class SwimmerHasAllergy implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "allergyDetectionDate")
    @Temporal(TemporalType.DATE)
    private Date allergyDetectionDate;
    @Size(max = 200)
    @Column(name = "observations")
    private String observations;
    @JoinColumn(name = "allergy_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Allergy allergyId;
    @JoinColumn(name = "AllergyStatus_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Allergystatus allergyStatusid;
    @JoinColumn(name = "swimmer_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Swimmer swimmerId;

    public SwimmerHasAllergy() {
    }

    public SwimmerHasAllergy(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getAllergyDetectionDate() {
        return allergyDetectionDate;
    }

    public void setAllergyDetectionDate(Date allergyDetectionDate) {
        this.allergyDetectionDate = allergyDetectionDate;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Allergy getAllergyId() {
        return allergyId;
    }

    public void setAllergyId(Allergy allergyId) {
        this.allergyId = allergyId;
    }

    public Allergystatus getAllergyStatusid() {
        return allergyStatusid;
    }

    public void setAllergyStatusid(Allergystatus allergyStatusid) {
        this.allergyStatusid = allergyStatusid;
    }

    public Swimmer getSwimmerId() {
        return swimmerId;
    }

    public void setSwimmerId(Swimmer swimmerId) {
        this.swimmerId = swimmerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SwimmerHasAllergy)) {
            return false;
        }
        SwimmerHasAllergy other = (SwimmerHasAllergy) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.SwimmerHasAllergy[ id=" + id + " ]";
    }
    
}
