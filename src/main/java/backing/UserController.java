package backing;

import backing.util.MobilePageController;
import entity.User;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "userController")
@ViewScoped
public class UserController extends AbstractController<User> {

    @Inject
    private MobilePageController mobilePageController;

    public UserController() {
        // Inform the Abstract parent controller of the concrete User Entity
        super(User.class);
    }

    /**
     * Sets the "items" attribute with a collection of UserHasRole entities that
     * are retrieved from User?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for UserHasRole page
     */
    public String navigateUserHasRoleList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("UserHasRole_items", this.getSelected().getUserHasRoleList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/userHasRole/index";
    }

    /**
     * Sets the "items" attribute with a collection of Swimmer entities that are
     * retrieved from User?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Swimmer page
     */
    public String navigateSwimmerList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Swimmer_items", this.getSelected().getSwimmerList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmer/index";
    }

}
