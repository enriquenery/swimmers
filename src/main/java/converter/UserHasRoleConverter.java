package converter;

import entity.UserHasRole;
import session.UserHasRoleFacade;
import backing.util.JsfUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.inject.spi.CDI;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

@FacesConverter(value = "userHasRoleConverter")
public class UserHasRoleConverter implements Converter {

    public UserHasRoleConverter() {
         this.ejbFacade = CDI.current().select(UserHasRoleFacade.class).get();
    }

    @Inject
    private UserHasRoleFacade ejbFacade;

    private static final String SEPARATOR = "#";
    private static final String SEPARATOR_ESCAPED = "\\#";

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.length() == 0 || JsfUtil.isDummySelectItem(component, value)) {
            return null;
        }
        return this.ejbFacade.find(getKey(value));
    }

    entity.UserHasRolePK getKey(String value) {
        entity.UserHasRolePK key;
        String values[] = value.split(SEPARATOR_ESCAPED);
        key = new entity.UserHasRolePK();
        key.setUserId(Integer.parseInt(values[0]));
        key.setRoleId(Integer.parseInt(values[1]));
        return key;
    }

    String getStringKey(entity.UserHasRolePK value) {
        StringBuffer sb = new StringBuffer();
        sb.append(value.getUserId());
        sb.append(SEPARATOR);
        sb.append(value.getRoleId());
        return sb.toString();
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
        if (object == null
                || (object instanceof String && ((String) object).length() == 0)) {
            return null;
        }
        if (object instanceof UserHasRole) {
            UserHasRole o = (UserHasRole) object;
            return getStringKey(o.getUserHasRolePK());
        } else {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), UserHasRole.class.getName()});
            return null;
        }
    }

}
