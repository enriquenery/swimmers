/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "deseasestatus")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Deseasestatus.findAll", query = "SELECT d FROM Deseasestatus d")
    , @NamedQuery(name = "Deseasestatus.findById", query = "SELECT d FROM Deseasestatus d WHERE d.id = :id")
    , @NamedQuery(name = "Deseasestatus.findByName", query = "SELECT d FROM Deseasestatus d WHERE d.name = :name")
    , @NamedQuery(name = "Deseasestatus.findByDescription", query = "SELECT d FROM Deseasestatus d WHERE d.description = :description")})
public class Deseasestatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @Size(max = 200)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "deseaseStatusid")
    private List<SwimmerHasDesease> swimmerHasDeseaseList;

    public Deseasestatus() {
    }

    public Deseasestatus(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public List<SwimmerHasDesease> getSwimmerHasDeseaseList() {
        return swimmerHasDeseaseList;
    }

    public void setSwimmerHasDeseaseList(List<SwimmerHasDesease> swimmerHasDeseaseList) {
        this.swimmerHasDeseaseList = swimmerHasDeseaseList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Deseasestatus)) {
            return false;
        }
        Deseasestatus other = (Deseasestatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Deseasestatus[ id=" + id + " ]";
    }
    
}
