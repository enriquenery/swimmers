/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "user_has_role")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserHasRole.findAll", query = "SELECT u FROM UserHasRole u")
    , @NamedQuery(name = "UserHasRole.findByUserId", query = "SELECT u FROM UserHasRole u WHERE u.userHasRolePK.userId = :userId")
    , @NamedQuery(name = "UserHasRole.findByRoleId", query = "SELECT u FROM UserHasRole u WHERE u.userHasRolePK.roleId = :roleId")
    , @NamedQuery(name = "UserHasRole.findByDate", query = "SELECT u FROM UserHasRole u WHERE u.date = :date")
    , @NamedQuery(name = "UserHasRole.findByActive", query = "SELECT u FROM UserHasRole u WHERE u.active = :active")
    , @NamedQuery(name = "UserHasRole.findByDateClosed", query = "SELECT u FROM UserHasRole u WHERE u.dateClosed = :dateClosed")})
public class UserHasRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserHasRolePK userHasRolePK;
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "dateClosed")
    @Temporal(TemporalType.DATE)
    private Date dateClosed;
    @JoinColumn(name = "role_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Role role;
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;

    public UserHasRole() {
    }

    public UserHasRole(UserHasRolePK userHasRolePK) {
        this.userHasRolePK = userHasRolePK;
    }

    public UserHasRole(int userId, int roleId) {
        this.userHasRolePK = new UserHasRolePK(userId, roleId);
    }

    public UserHasRolePK getUserHasRolePK() {
        return userHasRolePK;
    }

    public void setUserHasRolePK(UserHasRolePK userHasRolePK) {
        this.userHasRolePK = userHasRolePK;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(Date dateClosed) {
        this.dateClosed = dateClosed;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userHasRolePK != null ? userHasRolePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHasRole)) {
            return false;
        }
        UserHasRole other = (UserHasRole) object;
        if ((this.userHasRolePK == null && other.userHasRolePK != null) || (this.userHasRolePK != null && !this.userHasRolePK.equals(other.userHasRolePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UserHasRole[ userHasRolePK=" + userHasRolePK + " ]";
    }
    
}
