package backing;

import backing.util.MobilePageController;
import entity.Time;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "timeController")
@ViewScoped
public class TimeController extends AbstractController<Time> {

    @Inject
    private HitController hitidController;
    @Inject
    private MobilePageController mobilePageController;

    public TimeController() {
        // Inform the Abstract parent controller of the concrete Time Entity
        super(Time.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        hitidController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Hit controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareHitid(ActionEvent event) {
        if (this.getSelected() != null && hitidController.getSelected() == null) {
            hitidController.setSelected(this.getSelected().getHitid());
        }
    }
}
