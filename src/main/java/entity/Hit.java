/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "hit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hit.findAll", query = "SELECT h FROM Hit h")
    , @NamedQuery(name = "Hit.findById", query = "SELECT h FROM Hit h WHERE h.id = :id")
    , @NamedQuery(name = "Hit.findByHitNumber", query = "SELECT h FROM Hit h WHERE h.hitNumber = :hitNumber")
    , @NamedQuery(name = "Hit.findByLane", query = "SELECT h FROM Hit h WHERE h.lane = :lane")})
public class Hit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Column(name = "hitNumber")
    private Integer hitNumber;
    @Column(name = "lane")
    private Integer lane;
    @JoinColumn(name = "Event_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Event eventid;
    @JoinColumn(name = "hitStatus_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Hitstatus hitStatusid;
    @JoinColumn(name = "penalty_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Penalty penaltyId;
    @JoinColumn(name = "Swimmer_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Swimmer swimmerid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hitid")
    private List<Time> timeList;

    public Hit() {
    }

    public Hit(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHitNumber() {
        return hitNumber;
    }

    public void setHitNumber(Integer hitNumber) {
        this.hitNumber = hitNumber;
    }

    public Integer getLane() {
        return lane;
    }

    public void setLane(Integer lane) {
        this.lane = lane;
    }

    public Event getEventid() {
        return eventid;
    }

    public void setEventid(Event eventid) {
        this.eventid = eventid;
    }

    public Hitstatus getHitStatusid() {
        return hitStatusid;
    }

    public void setHitStatusid(Hitstatus hitStatusid) {
        this.hitStatusid = hitStatusid;
    }

    public Penalty getPenaltyId() {
        return penaltyId;
    }

    public void setPenaltyId(Penalty penaltyId) {
        this.penaltyId = penaltyId;
    }

    public Swimmer getSwimmerid() {
        return swimmerid;
    }

    public void setSwimmerid(Swimmer swimmerid) {
        this.swimmerid = swimmerid;
    }

    @XmlTransient
    public List<Time> getTimeList() {
        return timeList;
    }

    public void setTimeList(List<Time> timeList) {
        this.timeList = timeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hit)) {
            return false;
        }
        Hit other = (Hit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Hit[ id=" + id + " ]";
    }
    
}
