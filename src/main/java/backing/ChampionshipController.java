package backing;

import backing.util.MobilePageController;
import entity.Championship;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "championshipController")
@ViewScoped
public class ChampionshipController extends AbstractController<Championship> {

    @Inject
    private MobilePageController mobilePageController;

    public ChampionshipController() {
        // Inform the Abstract parent controller of the concrete Championship Entity
        super(Championship.class);
    }

    /**
     * Sets the "items" attribute with a collection of Event entities that are
     * retrieved from Championship?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Event page
     */
    public String navigateEventList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Event_items", this.getSelected().getEventList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/event/index";
    }

}
