package backing;

import backing.util.MobilePageController;
import entity.Hit;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "hitController")
@ViewScoped
public class HitController extends AbstractController<Hit> {

    @Inject
    private EventController eventidController;
    @Inject
    private HitstatusController hitStatusidController;
    @Inject
    private PenaltyController penaltyIdController;
    @Inject
    private SwimmerController swimmeridController;
    @Inject
    private MobilePageController mobilePageController;

    public HitController() {
        // Inform the Abstract parent controller of the concrete Hit Entity
        super(Hit.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        eventidController.setSelected(null);
        hitStatusidController.setSelected(null);
        penaltyIdController.setSelected(null);
        swimmeridController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Event controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareEventid(ActionEvent event) {
        if (this.getSelected() != null && eventidController.getSelected() == null) {
            eventidController.setSelected(this.getSelected().getEventid());
        }
    }

    /**
     * Sets the "selected" attribute of the Hitstatus controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareHitStatusid(ActionEvent event) {
        if (this.getSelected() != null && hitStatusidController.getSelected() == null) {
            hitStatusidController.setSelected(this.getSelected().getHitStatusid());
        }
    }

    /**
     * Sets the "selected" attribute of the Penalty controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void preparePenaltyId(ActionEvent event) {
        if (this.getSelected() != null && penaltyIdController.getSelected() == null) {
            penaltyIdController.setSelected(this.getSelected().getPenaltyId());
        }
    }

    /**
     * Sets the "selected" attribute of the Swimmer controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareSwimmerid(ActionEvent event) {
        if (this.getSelected() != null && swimmeridController.getSelected() == null) {
            swimmeridController.setSelected(this.getSelected().getSwimmerid());
        }
    }

    /**
     * Sets the "items" attribute with a collection of Time entities that are
     * retrieved from Hit?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Time page
     */
    public String navigateTimeList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Time_items", this.getSelected().getTimeList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/time/index";
    }

}
