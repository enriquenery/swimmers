package backing;

import backing.util.MobilePageController;
import entity.Race;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "raceController")
@ViewScoped
public class RaceController extends AbstractController<Race> {

    @Inject
    private MobilePageController mobilePageController;

    public RaceController() {
        // Inform the Abstract parent controller of the concrete Race Entity
        super(Race.class);
    }

    /**
     * Sets the "items" attribute with a collection of Event entities that are
     * retrieved from Race?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Event page
     */
    public String navigateEventList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Event_items", this.getSelected().getEventList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/event/index";
    }

}
