package backing;

import backing.util.MobilePageController;
import entity.Deseasestatus;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "deseasestatusController")
@ViewScoped
public class DeseasestatusController extends AbstractController<Deseasestatus> {

    @Inject
    private MobilePageController mobilePageController;

    public DeseasestatusController() {
        // Inform the Abstract parent controller of the concrete Deseasestatus Entity
        super(Deseasestatus.class);
    }

    /**
     * Sets the "items" attribute with a collection of SwimmerHasDesease
     * entities that are retrieved from Deseasestatus?cap_first and returns the
     * navigation outcome.
     *
     * @return navigation outcome for SwimmerHasDesease page
     */
    public String navigateSwimmerHasDeseaseList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SwimmerHasDesease_items", this.getSelected().getSwimmerHasDeseaseList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmerHasDesease/index";
    }

}
