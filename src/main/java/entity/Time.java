/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "time")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Time.findAll", query = "SELECT t FROM Time t")
    , @NamedQuery(name = "Time.findById", query = "SELECT t FROM Time t WHERE t.id = :id")
    , @NamedQuery(name = "Time.findByLap", query = "SELECT t FROM Time t WHERE t.lap = :lap")
    , @NamedQuery(name = "Time.findByElapsedTime", query = "SELECT t FROM Time t WHERE t.elapsedTime = :elapsedTime")})
public class Time implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "lap")
    private Integer lap;
    @Column(name = "elapsedTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date elapsedTime;
    @JoinColumn(name = "Hit_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Hit hitid;

    public Time() {
    }

    public Time(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLap() {
        return lap;
    }

    public void setLap(Integer lap) {
        this.lap = lap;
    }

    public Date getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(Date elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public Hit getHitid() {
        return hitid;
    }

    public void setHitid(Hit hitid) {
        this.hitid = hitid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Time)) {
            return false;
        }
        Time other = (Time) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Time[ id=" + id + " ]";
    }
    
}
