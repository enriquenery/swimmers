-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema swimmers
-- -----------------------------------------------------
-- Database created for the timming system and swimmers registry.
-- 
-- 
-- 
DROP SCHEMA IF EXISTS `swimmers` ;

-- -----------------------------------------------------
-- Schema swimmers
--
-- Database created for the timming system and swimmers registry.
-- 
-- 
-- 
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `swimmers` DEFAULT CHARACTER SET utf8 ;
USE `swimmers` ;

-- -----------------------------------------------------
-- Table `swimmers`.`Role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Role` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `roleName` VARCHAR(50) NULL,
  `description` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`User` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userProfilePhoto` BLOB NULL,
  `name` VARCHAR(50) NULL,
  `surname` VARCHAR(50) NULL,
  `userName` VARCHAR(25) NULL,
  `password` VARCHAR(25) NULL,
  `email` VARCHAR(50) NULL,
  `birthdate` DATE NULL,
  `gender` CHAR(1) NULL,
  `carnet` VARCHAR(7) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'It stores all the users basic information needed for the system\'s correct work.\n';


-- -----------------------------------------------------
-- Table `swimmers`.`Faculty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Faculty` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Faculty` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `university` VARCHAR(50) NULL,
  `location` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`Swimmer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Swimmer` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Swimmer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Faculty_id` INT NOT NULL,
  `User_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Swimmer_Faculty1`
    FOREIGN KEY (`Faculty_id`)
    REFERENCES `swimmers`.`Faculty` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Swimmer_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `swimmers`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'It stores all the swimmers basic information needed for the system\'s correct work.';

CREATE INDEX `fk_Swimmer_Faculty1_idx` ON `swimmers`.`Swimmer` (`Faculty_id` ASC);

CREATE INDEX `fk_Swimmer_User1_idx` ON `swimmers`.`Swimmer` (`User_id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`Level`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Level` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Level` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `description` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `swimmers`.`Level` (`id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`Swimmer_has_Level`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Swimmer_has_Level` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Swimmer_has_Level` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Swimmer_id` INT NOT NULL,
  `Level_id` INT NOT NULL,
  `dateStart` DATE NULL,
  `dateEnd` DATE NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Swimmers_has_Level_Swimmers`
    FOREIGN KEY (`Swimmer_id`)
    REFERENCES `swimmers`.`Swimmer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Swimmers_has_Level_Level1`
    FOREIGN KEY (`Level_id`)
    REFERENCES `swimmers`.`Level` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Swimmers_has_Level_level_idx` ON `swimmers`.`Swimmer_has_Level` (`Level_id` ASC);

CREATE INDEX `fk_Swimmers_has_Level_swimmer_idx` ON `swimmers`.`Swimmer_has_Level` (`Swimmer_id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`SwimmingClub`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`SwimmingClub` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`SwimmingClub` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `clubName` VARCHAR(100) NULL,
  `clubDescription` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`Swimmer_has_SwimmingClub`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Swimmer_has_SwimmingClub` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Swimmer_has_SwimmingClub` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Swimmer_id` INT NOT NULL,
  `swimmingClub_id` INT NOT NULL,
  `dateStart` DATE NULL,
  `dateEnd` DATE NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Swimmer_has_SwimmingClub_Swimmer1`
    FOREIGN KEY (`Swimmer_id`)
    REFERENCES `swimmers`.`Swimmer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Swimmer_has_SwimmingClub_SwimmingClub1`
    FOREIGN KEY (`swimmingClub_id`)
    REFERENCES `swimmers`.`SwimmingClub` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Swimmer_has_SwimmingClub_swimmingClub_idx` ON `swimmers`.`Swimmer_has_SwimmingClub` (`swimmingClub_id` ASC);

CREATE INDEX `fk_Swimmer_has_SwimmingClub_swimmer_idx` ON `swimmers`.`Swimmer_has_SwimmingClub` (`Swimmer_id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`Allergy`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Allergy` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Allergy` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `description` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`Injury`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Injury` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Injury` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `description` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`Desease`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Desease` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Desease` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `description` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`InjuryStatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`InjuryStatus` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`InjuryStatus` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `description` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `swimmers`.`InjuryStatus` (`id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`Swimmer_has_Injury`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Swimmer_has_Injury` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Swimmer_has_Injury` (
  `int` INT NOT NULL AUTO_INCREMENT,
  `swimmer_id` INT NOT NULL,
  `injury_id` INT NOT NULL,
  `injuryDate` DATE NULL,
  `observations` VARCHAR(200) NULL,
  `InjuryStatus_id` INT NOT NULL,
  PRIMARY KEY (`int`),
  CONSTRAINT `fk_Swimmer_has_Injury_Swimmer1`
    FOREIGN KEY (`swimmer_id`)
    REFERENCES `swimmers`.`Swimmer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Swimmer_has_Injury_Injury1`
    FOREIGN KEY (`injury_id`)
    REFERENCES `swimmers`.`Injury` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Swimmer_has_Injury_InjuryStatus1`
    FOREIGN KEY (`InjuryStatus_id`)
    REFERENCES `swimmers`.`InjuryStatus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Swimmer_has_Injury_injury_idx` ON `swimmers`.`Swimmer_has_Injury` (`injury_id` ASC);

CREATE INDEX `fk_Swimmer_has_Injury_swimmer_idx` ON `swimmers`.`Swimmer_has_Injury` (`swimmer_id` ASC);

CREATE INDEX `fk_Swimmer_has_Injury_InjuryStatus1_idx` ON `swimmers`.`Swimmer_has_Injury` (`InjuryStatus_id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`AllergyStatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`AllergyStatus` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`AllergyStatus` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `description` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`Swimmer_has_Allergy`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Swimmer_has_Allergy` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Swimmer_has_Allergy` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `swimmer_id` INT NOT NULL,
  `allergy_id` INT NOT NULL,
  `allergyDetectionDate` DATE NULL,
  `observations` VARCHAR(200) NULL,
  `AllergyStatus_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Swimmer_has_Allergy_Swimmer1`
    FOREIGN KEY (`swimmer_id`)
    REFERENCES `swimmers`.`Swimmer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Swimmer_has_Allergy_Allergy1`
    FOREIGN KEY (`allergy_id`)
    REFERENCES `swimmers`.`Allergy` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Swimmer_has_Allergy_AllergyStatus1`
    FOREIGN KEY (`AllergyStatus_id`)
    REFERENCES `swimmers`.`AllergyStatus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Swimmer_has_Allergy_allergy_idx` ON `swimmers`.`Swimmer_has_Allergy` (`allergy_id` ASC);

CREATE INDEX `fk_Swimmer_has_Allergy_swimmer_idx` ON `swimmers`.`Swimmer_has_Allergy` (`swimmer_id` ASC);

CREATE INDEX `fk_Swimmer_has_Allergy_AllergyStatus1_idx` ON `swimmers`.`Swimmer_has_Allergy` (`AllergyStatus_id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`DeseaseStatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`DeseaseStatus` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`DeseaseStatus` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `description` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`Swimmer_has_Desease`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Swimmer_has_Desease` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Swimmer_has_Desease` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `swimmer_id` INT NOT NULL,
  `desease_id` INT NOT NULL,
  `deseaseDetectionDate` DATE NULL,
  `observations` VARCHAR(200) NULL,
  `DeseaseStatus_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Swimmer_has_Desease_Swimmer1`
    FOREIGN KEY (`swimmer_id`)
    REFERENCES `swimmers`.`Swimmer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Swimmer_has_Desease_Desease1`
    FOREIGN KEY (`desease_id`)
    REFERENCES `swimmers`.`Desease` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Swimmer_has_Desease_DeseaseStatus1`
    FOREIGN KEY (`DeseaseStatus_id`)
    REFERENCES `swimmers`.`DeseaseStatus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Swimmer_has_Desease_desease_idx` ON `swimmers`.`Swimmer_has_Desease` (`desease_id` ASC);

CREATE INDEX `fk_Swimmer_has_Desease_swimmer_idx` ON `swimmers`.`Swimmer_has_Desease` (`swimmer_id` ASC);

CREATE INDEX `fk_Swimmer_has_Desease_DeseaseStatus1_idx` ON `swimmers`.`Swimmer_has_Desease` (`DeseaseStatus_id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`User_has_Role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`User_has_Role` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`User_has_Role` (
  `user_id` INT NOT NULL,
  `role_id` INT NOT NULL,
  `date` DATE NULL COMMENT 'Date in wich the role is created.',
  `active` TINYINT(1) NULL COMMENT 'Defines if the user is under the role asigned. ',
  `dateClosed` DATE NULL COMMENT 'The date when the role was unassigned from the user.',
  PRIMARY KEY (`user_id`, `role_id`),
  CONSTRAINT `fk_User_has_Role_User1`
    FOREIGN KEY (`user_id`)
    REFERENCES `swimmers`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Role_Role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `swimmers`.`Role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_User_has_Role_role_idx` ON `swimmers`.`User_has_Role` (`role_id` ASC);

CREATE INDEX `fk_User_has_Role_user_idx` ON `swimmers`.`User_has_Role` (`user_id` ASC);

CREATE UNIQUE INDEX `user_role_uniqueness` ON `swimmers`.`User_has_Role` (`user_id` ASC, `role_id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`Race`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Race` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Race` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `description` VARCHAR(100) NULL,
  `gender` CHAR(1) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`Penalty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Penalty` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Penalty` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `description` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`HitStatus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`HitStatus` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`HitStatus` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NULL,
  `description` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`Championship`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Championship` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Championship` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `description` VARCHAR(250) NULL,
  `date` DATE NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `swimmers`.`Event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Event` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Event` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `eventStatus` TINYINT(1) NULL COMMENT 'Defines the status of the event: completed or cancelled.',
  `Championship_id` INT NOT NULL,
  `Race_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Event_Championship1`
    FOREIGN KEY (`Championship_id`)
    REFERENCES `swimmers`.`Championship` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Event_Race1`
    FOREIGN KEY (`Race_id`)
    REFERENCES `swimmers`.`Race` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Event_Championship1_idx` ON `swimmers`.`Event` (`Championship_id` ASC);

CREATE INDEX `fk_Event_Race1_idx` ON `swimmers`.`Event` (`Race_id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`Hit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Hit` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Hit` (
  `id` INT NOT NULL,
  `hitNumber` INT NULL,
  `lane` INT NULL,
  `hitStatus_id` INT NOT NULL,
  `penalty_id` INT NOT NULL,
  `Event_id` INT NOT NULL,
  `Swimmer_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Hit_Penalty1`
    FOREIGN KEY (`penalty_id`)
    REFERENCES `swimmers`.`Penalty` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Hit_HitStatus1`
    FOREIGN KEY (`hitStatus_id`)
    REFERENCES `swimmers`.`HitStatus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Hit_Event1`
    FOREIGN KEY (`Event_id`)
    REFERENCES `swimmers`.`Event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Hit_Swimmer1`
    FOREIGN KEY (`Swimmer_id`)
    REFERENCES `swimmers`.`Swimmer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Hit_penalty_idx` ON `swimmers`.`Hit` (`penalty_id` ASC);

CREATE INDEX `fk_Hit_hitstatus_idx` ON `swimmers`.`Hit` (`hitStatus_id` ASC);

CREATE INDEX `fk_Hit_Event1_idx` ON `swimmers`.`Hit` (`Event_id` ASC);

CREATE INDEX `fk_Hit_Swimmer1_idx` ON `swimmers`.`Hit` (`Swimmer_id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`IMC`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`IMC` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`IMC` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NULL,
  `weight` DECIMAL(5,2) NULL,
  `height` DECIMAL(5,2) NULL,
  `imc` DECIMAL(5,2) NULL,
  `Swimmer_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_IMC_Swimmer1`
    FOREIGN KEY (`Swimmer_id`)
    REFERENCES `swimmers`.`Swimmer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `IMC_uniqueness` ON `swimmers`.`IMC` (`date` ASC, `weight` ASC, `height` ASC, `imc` ASC);

CREATE UNIQUE INDEX `id_UNIQUE` ON `swimmers`.`IMC` (`id` ASC);

CREATE INDEX `fk_IMC_Swimmer1_idx` ON `swimmers`.`IMC` (`Swimmer_id` ASC);


-- -----------------------------------------------------
-- Table `swimmers`.`Time`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `swimmers`.`Time` ;

CREATE TABLE IF NOT EXISTS `swimmers`.`Time` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `lap` INT NULL,
  `elapsedTime` DATETIME NULL,
  `Hit_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Time_Hit1`
    FOREIGN KEY (`Hit_id`)
    REFERENCES `swimmers`.`Hit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Time_Hit1_idx` ON `swimmers`.`Time` (`Hit_id` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
