/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "swimmer_has_desease")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SwimmerHasDesease.findAll", query = "SELECT s FROM SwimmerHasDesease s")
    , @NamedQuery(name = "SwimmerHasDesease.findById", query = "SELECT s FROM SwimmerHasDesease s WHERE s.id = :id")
    , @NamedQuery(name = "SwimmerHasDesease.findByDeseaseDetectionDate", query = "SELECT s FROM SwimmerHasDesease s WHERE s.deseaseDetectionDate = :deseaseDetectionDate")
    , @NamedQuery(name = "SwimmerHasDesease.findByObservations", query = "SELECT s FROM SwimmerHasDesease s WHERE s.observations = :observations")})
public class SwimmerHasDesease implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "deseaseDetectionDate")
    @Temporal(TemporalType.DATE)
    private Date deseaseDetectionDate;
    @Size(max = 200)
    @Column(name = "observations")
    private String observations;
    @JoinColumn(name = "desease_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Desease deseaseId;
    @JoinColumn(name = "DeseaseStatus_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Deseasestatus deseaseStatusid;
    @JoinColumn(name = "swimmer_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Swimmer swimmerId;

    public SwimmerHasDesease() {
    }

    public SwimmerHasDesease(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDeseaseDetectionDate() {
        return deseaseDetectionDate;
    }

    public void setDeseaseDetectionDate(Date deseaseDetectionDate) {
        this.deseaseDetectionDate = deseaseDetectionDate;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Desease getDeseaseId() {
        return deseaseId;
    }

    public void setDeseaseId(Desease deseaseId) {
        this.deseaseId = deseaseId;
    }

    public Deseasestatus getDeseaseStatusid() {
        return deseaseStatusid;
    }

    public void setDeseaseStatusid(Deseasestatus deseaseStatusid) {
        this.deseaseStatusid = deseaseStatusid;
    }

    public Swimmer getSwimmerId() {
        return swimmerId;
    }

    public void setSwimmerId(Swimmer swimmerId) {
        this.swimmerId = swimmerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SwimmerHasDesease)) {
            return false;
        }
        SwimmerHasDesease other = (SwimmerHasDesease) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.SwimmerHasDesease[ id=" + id + " ]";
    }
    
}
