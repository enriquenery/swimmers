package backing;

import backing.util.MobilePageController;
import entity.Role;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "roleController")
@ViewScoped
public class RoleController extends AbstractController<Role> {

    @Inject
    private MobilePageController mobilePageController;

    public RoleController() {
        // Inform the Abstract parent controller of the concrete Role Entity
        super(Role.class);
    }

    /**
     * Sets the "items" attribute with a collection of UserHasRole entities that
     * are retrieved from Role?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for UserHasRole page
     */
    public String navigateUserHasRoleList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("UserHasRole_items", this.getSelected().getUserHasRoleList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/userHasRole/index";
    }

}
