package backing;

import backing.util.MobilePageController;
import entity.Imc;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "imcController")
@ViewScoped
public class ImcController extends AbstractController<Imc> {

    @Inject
    private SwimmerController swimmeridController;
    @Inject
    private MobilePageController mobilePageController;

    public ImcController() {
        // Inform the Abstract parent controller of the concrete Imc Entity
        super(Imc.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        swimmeridController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Swimmer controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareSwimmerid(ActionEvent event) {
        if (this.getSelected() != null && swimmeridController.getSelected() == null) {
            swimmeridController.setSelected(this.getSelected().getSwimmerid());
        }
    }
}
