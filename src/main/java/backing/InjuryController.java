package backing;

import backing.util.MobilePageController;
import entity.Injury;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "injuryController")
@ViewScoped
public class InjuryController extends AbstractController<Injury> {

    @Inject
    private MobilePageController mobilePageController;

    public InjuryController() {
        // Inform the Abstract parent controller of the concrete Injury Entity
        super(Injury.class);
    }

    /**
     * Sets the "items" attribute with a collection of SwimmerHasInjury entities
     * that are retrieved from Injury?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for SwimmerHasInjury page
     */
    public String navigateSwimmerHasInjuryList() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("SwimmerHasInjury_items", this.getSelected().getSwimmerHasInjuryList());
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/maintenance/swimmerHasInjury/index";
    }

}
