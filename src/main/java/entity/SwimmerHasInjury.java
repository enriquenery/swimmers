/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author enriq
 */
@Entity
@Table(name = "swimmer_has_injury")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SwimmerHasInjury.findAll", query = "SELECT s FROM SwimmerHasInjury s")
    , @NamedQuery(name = "SwimmerHasInjury.findByInt1", query = "SELECT s FROM SwimmerHasInjury s WHERE s.int1 = :int1")
    , @NamedQuery(name = "SwimmerHasInjury.findByInjuryDate", query = "SELECT s FROM SwimmerHasInjury s WHERE s.injuryDate = :injuryDate")
    , @NamedQuery(name = "SwimmerHasInjury.findByObservations", query = "SELECT s FROM SwimmerHasInjury s WHERE s.observations = :observations")})
public class SwimmerHasInjury implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "int")
    private Integer int1;
    @Column(name = "injuryDate")
    @Temporal(TemporalType.DATE)
    private Date injuryDate;
    @Size(max = 200)
    @Column(name = "observations")
    private String observations;
    @JoinColumn(name = "injury_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Injury injuryId;
    @JoinColumn(name = "InjuryStatus_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Injurystatus injuryStatusid;
    @JoinColumn(name = "swimmer_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Swimmer swimmerId;

    public SwimmerHasInjury() {
    }

    public SwimmerHasInjury(Integer int1) {
        this.int1 = int1;
    }

    public Integer getInt1() {
        return int1;
    }

    public void setInt1(Integer int1) {
        this.int1 = int1;
    }

    public Date getInjuryDate() {
        return injuryDate;
    }

    public void setInjuryDate(Date injuryDate) {
        this.injuryDate = injuryDate;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Injury getInjuryId() {
        return injuryId;
    }

    public void setInjuryId(Injury injuryId) {
        this.injuryId = injuryId;
    }

    public Injurystatus getInjuryStatusid() {
        return injuryStatusid;
    }

    public void setInjuryStatusid(Injurystatus injuryStatusid) {
        this.injuryStatusid = injuryStatusid;
    }

    public Swimmer getSwimmerId() {
        return swimmerId;
    }

    public void setSwimmerId(Swimmer swimmerId) {
        this.swimmerId = swimmerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int1 != null ? int1.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SwimmerHasInjury)) {
            return false;
        }
        SwimmerHasInjury other = (SwimmerHasInjury) object;
        if ((this.int1 == null && other.int1 != null) || (this.int1 != null && !this.int1.equals(other.int1))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.SwimmerHasInjury[ int1=" + int1 + " ]";
    }
    
}
