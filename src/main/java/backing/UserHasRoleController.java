package backing;

import backing.util.MobilePageController;
import entity.UserHasRole;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "userHasRoleController")
@ViewScoped
public class UserHasRoleController extends AbstractController<UserHasRole> {

    @Inject
    private RoleController roleController;
    @Inject
    private UserController userController;
    @Inject
    private MobilePageController mobilePageController;

    public UserHasRoleController() {
        // Inform the Abstract parent controller of the concrete UserHasRole Entity
        super(UserHasRole.class);
    }

    @Override
    protected void setEmbeddableKeys() {
        this.getSelected().getUserHasRolePK().setUserId(this.getSelected().getUser().getId());
        this.getSelected().getUserHasRolePK().setRoleId(this.getSelected().getRole().getId());
    }

    @Override
    protected void initializeEmbeddableKey() {
        this.getSelected().setUserHasRolePK(new entity.UserHasRolePK());
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        roleController.setSelected(null);
        userController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Role controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareRole(ActionEvent event) {
        if (this.getSelected() != null && roleController.getSelected() == null) {
            roleController.setSelected(this.getSelected().getRole());
        }
    }

    /**
     * Sets the "selected" attribute of the User controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareUser(ActionEvent event) {
        if (this.getSelected() != null && userController.getSelected() == null) {
            userController.setSelected(this.getSelected().getUser());
        }
    }
}
