package backing;

import backing.util.MobilePageController;
import entity.SwimmerHasLevel;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "swimmerHasLevelController")
@ViewScoped
public class SwimmerHasLevelController extends AbstractController<SwimmerHasLevel> {

    @Inject
    private LevelController levelidController;
    @Inject
    private SwimmerController swimmeridController;
    @Inject
    private MobilePageController mobilePageController;

    public SwimmerHasLevelController() {
        // Inform the Abstract parent controller of the concrete SwimmerHasLevel Entity
        super(SwimmerHasLevel.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        levelidController.setSelected(null);
        swimmeridController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Level controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareLevelid(ActionEvent event) {
        if (this.getSelected() != null && levelidController.getSelected() == null) {
            levelidController.setSelected(this.getSelected().getLevelid());
        }
    }

    /**
     * Sets the "selected" attribute of the Swimmer controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareSwimmerid(ActionEvent event) {
        if (this.getSelected() != null && swimmeridController.getSelected() == null) {
            swimmeridController.setSelected(this.getSelected().getSwimmerid());
        }
    }
}
